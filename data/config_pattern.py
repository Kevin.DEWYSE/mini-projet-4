pattern_args = {
    "help":"For burn forest",
    "arguments":[
        {"name":"-rows","help":"Number of rows","attribute":"rows","default":3,"type":int},
        {"name":"-cols","help":"Number of columns","attribute":"cols","default":3,"type":int},
        {"name":"-cell_size","help":"Cell_Size :/","attribute":"cell_size","default":35,"type":int},
        {"name":"-afforestation","help":"Pourcentage of Forest","attribute":"afforestation","default":0.6,"type":float}
    ]
}