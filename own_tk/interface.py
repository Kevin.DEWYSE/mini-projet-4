import tkinter as tk

l = [[0, 1, 1, 0], [0, 1, 0, 1], [0, 1, 1, 1], [0, 1, 0, 1]]
n = len(l)      # à modifier
lngt = 400 // n #dimension des carrés voulus
W = 400
H = 400

#Création et définition de la taille de la fenêtre
fen = tk.Tk()
fen.geometry("%dx%d" % (W,H))

#Création du Canvas
can = tk.Canvas(fen, width=W, height=H)
can.pack(side=tk.LEFT)

def selection():
    pass

def affichage(can, forest):
    for i in range(n):
        y = i * lngt
        for j in range(n):
            x = j * lngt
            if forest[i][j] == 0:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="white")
            if forest[i][j] == 1:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="green")
            if forest[i][j] == 2:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="red")
            if forest[i][j] == 3:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="grey")

def verif_pause():
    pass

can.bind('<Button-1>',selection)
affichage(can, l)
fen.mainloop()