import argparse
"""
    Fonction Args
    %       Cette fonction récupère les arguments passés en ligne de commande
            et les initialise à une valeur par défaut si nécessaire.
    %IN     pattern_args : dictionnaire de paramètre pour initialiser le module
    %OUT    args : les arguments récoltés
"""
def Args(pattern_args):
    parser = argparse.ArgumentParser(description=pattern_args['help'])
    for data in pattern_args['arguments']:
        parser.add_argument(data['name'], help=data['help'])
    args = parser.parse_args()
    for data in pattern_args['arguments']:
        args_data = args.__dict__[data['attribute']]
        if args_data == None:
            args_data = data['default']
        elif type(args_data) != data['type']:
            args_data = data['type'](args_data)
        args.__dict__[data['attribute']] = args_data
    return args